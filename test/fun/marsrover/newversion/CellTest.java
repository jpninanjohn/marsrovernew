package fun.marsrover.newversion;

import org.junit.Test;

import static fun.marsrover.newversion.Direction.*;
import static org.junit.Assert.assertEquals;

/**
 * Created by ninan on 8/5/16.
 */
public class CellTest {

    @Test
    public void isTheCorrectneighbourreturnedWhenDirectionIsNorth() {
        Cell cell = new Cell(0, 1);
        Cell nextCell = cell.getNeighbourBasedOnDirection(N);
        assertEquals(new Cell(0, 2), nextCell);
    }

    @Test
    public void isTheCorrectneighbourreturnedWhenDirectionIsWest() {
        Cell cell = new Cell(1, 1);
        Cell nextCell = cell.getNeighbourBasedOnDirection(W);
        assertEquals(new Cell(0, 1), nextCell);
    }

    @Test
    public void isTheCorrectneighbourreturnedWhenDirectionIsEast() {
        Cell cell = new Cell(1, 1);
        Cell nextCell = cell.getNeighbourBasedOnDirection(E);
        assertEquals(new Cell(2, 1), nextCell);
    }

    @Test
    public void isTheCorrectneighbourreturnedWhenDirectionIsSouth() {
        Cell cell = new Cell(1, 1);
        Cell nextCell = cell.getNeighbourBasedOnDirection(S);
        assertEquals(new Cell(1, 0), nextCell);
    }
}