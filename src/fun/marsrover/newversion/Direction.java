package fun.marsrover.newversion;

/**
 * Created by ninan on 8/5/16.
 */
public enum Direction {
    N,
    S,
    E,
    W
}
